users = [
    {
        "ID": 1,
        "NAME": "Mak Mayo",
        "LOGIN": "admin",
        "PASSWORD": "admin",
        "EMAIL": "admin@host.com",
        "ROLE_ID": "ADMIN",
        "AVATAR_URL": "../img/avatars/1gh2jkhj3.jpg"
    },
    {
        "ID": 2,
        "NAME": "Chin Wong-Hu",
        "LOGIN": "manager",
        "PASSWORD": "manager",
        "EMAIL": "manager@host.com",
        "ROLE_ID": "MANAGER",
        "AVATAR_URL": "../img/avatars/2gh2jkhj3.jpg"
    },
    {
        "ID": 3,
        "NAME": "Frank Owusu",
        "LOGIN": "MANAGER",
        "PASSWORD": "frmanager",
        "EMAIL": "frankowusu@host.com",
        "ROLE_ID": "MANAGER",
        "AVATAR_URL": "../img/avatars/1gh2jkhj3.jpg"
    },
    {
        "ID": 4,
        "NAME": "Pioter Cristerson",
        "LOGIN": "user",
        "PASSWORD": "user",
        "EMAIL": "pioterpoljak@host.com",
        "ROLE_ID": "USER",
        "AVATAR_URL": "../img/avatars/1gh2jkhj3.jpg"
    },
    {
        "ID": 5,
        "NAME": "Simona Chamber",
        "LOGIN": "user",
        "PASSWORD": "simonka",
        "EMAIL": "chamsims@host.com",
        "ROLE_ID": "USER",
        "AVATAR_URL": "../img/avatars/1gh2jkhj3.jpg"
    },
    {
        "ID": 6,
        "NAME": "",
        "LOGIN": "",
        "PASSWORD": "",
        "EMAIL": "subscriber@host.com",
        "ROLE_ID": "SUBSCRIBER",
        "AVATAR_URL": ""
    },
]

issue = [
    {
        "ID": 1,
        "NAME": "Slippery road",
        "DESCRIPTION": "Some big description to this issue",
        "MAP_POINTER": "LatLng(50.62845, 26.2472)",
        "PRIORITY_ID": 1,
        "STATUS": "APPROVED",
        "CATEGORY_ID": "1",
        "ATTACHMENTS": "../img/attacments/incident00001.png"
    },
    {
        "ID": 2,
        "NAME": "Remove graffiti please",
        "DESCRIPTION": "Some big description to this issue",
        "MAP_POINTER": "LatLng(50.622647, 26.265234)",
        "PRIORITY_ID": 1,
        "STATUS": "TO_RESOLVE",
        "CATEGORY_ID": "1",
        "ATTACHMENTS": "../img/attacments/incident01272.png"
    },
    {
        "ID": 3,
        "NAME": "Rubish on the beach",
        "DESCRIPTION": "Some big description to this issue",
        "MAP_POINTER": "LatLng(50.59838, 26.25389)",
        "PRIORITY_ID": 1,
        "STATUS": "NEW",
        "CATEGORY_ID": "2",
        "ATTACHMENTS": "../img/attacments/incident00001.png"
    },
    {
        "ID": 4,
        "NAME": "Dead fish",
        "DESCRIPTION": "Some big description to this issue",
        "MAP_POINTER": "LatLng(50.66181, 26.16304)",
        "PRIORITY_ID": 1,
        "STATUS": "RESOLVED",
        "CATEGORY_ID": "2",
        "ATTACHMENTS": "../img/attacments/incident00001.png"
    },
]