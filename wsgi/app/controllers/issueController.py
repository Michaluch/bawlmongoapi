#!/usr/bin/env python

from app.models.issueModel import IssueModel

class IssueController(IssueModel):
    
    def get_all_issues(self, ):
        coursor = self.collection.find({})
        list_issues = []
        for key in coursor:
            del(key["_id"])
            list_issues.append(key)
        return list_issues
    
    
    def get_issue_by_atribute(self, **kwargs):
        coursor = self.collection.find_one(kwargs)
        if coursor:
            del(coursor["_id"])
        return coursor
    
    
    def get_id_issue(self, issue_id=None):
        return self.collection.find_one({'ID': issue_id})['ID']
    
    
    def get_issue_history(self, issue_id=None):
        return self.collection.find_one({'ID': issue_id})['HISTORY']
    
    
    def add_new_issue(self, issue=dict()):
        try:
            doc = dict()
            
            doc["ID"] = str(int(self.collection.find_one(sort=[('ID', -1)])['ID']) + 1)
            doc["NAME"] = issue.get('name') if 'name' in issue else None
            doc["DESCRIPTION"] = issue.get('desc') if 'desc' in issue else None
            doc["MAP_POINTER"] = issue.get('point') if 'point' in issue else None
            doc["PRIORITY_ID"] = issue.get('priority') if 'priority' in issue else None
            doc["STATUS"] = issue.get('status') if 'status' in issue else None
            doc["CATEGORY_ID"] = issue.get('category') if 'category' in issue else None
            doc["ATTACHMENTS"] = issue.get('attach') if 'attach' in issue else None
            doc["HISTORY"] = issue.get('history') if 'history' in issue else None
            
            self.collection.insert(doc)
            return doc
        except:
            return {"message": 'Error'}