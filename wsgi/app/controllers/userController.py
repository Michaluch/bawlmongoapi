#!/usr/bin/env python
from bson.objectid import ObjectId
import re, hashlib

from app.models.userModel import UserModel

class UserController(UserModel):
    _user = dict(name=None, email=None,
                 login=None, password=None,
                 role=0, avatar=None)
    
    
    def get_list_all_users(self, ):
        coursor = self.collection.find({})
        list_users = []
        for key in coursor:
            del(key["_id"])
            list_users.append(key)
        return list_users
    
    
    def get_user_by_atribute(self, **kwargs):
        coursor = self.collection.find_one(kwargs)
        del(coursor["_id"])
        return coursor
     
            
    def get_id_user(self, username=None):
        return self.collection.find_one({'LOGIN': username})['ID']
    
    
    def get_user_by_id(self, userid=0):
        query = {'ID': userid}
        return self.collection.find_one(query)['LOGIN']
        
    
    def is_user(self, username="admin", passwd="pasword"):
        coursor = self.collection.find_one({
                                            "LOGIN": username,
                                            "PASSWORD": passwd})
        try:
            if (coursor["LOGIN"] == username) and (coursor["PASSWORD"] == passwd):
                return True
        except:
            return False
    

    def add_new_user(self, user=dict()):

        try:
            doc = dict()
            if self._valid_username(user['login']):
                doc['LOGIN'] = user['login']
            else:
                return {"message": "Invalid username"}
            
            if self._valid_pass(user['password']):
                doc['PASSWORD'] = user['password']
            else:
                return {"message": "Invalid password"}
            
            if self._valid_email(user['email']):
                doc['EMAIL'] = user['email']
            else:
                return {"message": "Invalid email"}
            doc['NAME'] = user['name'] if 'name' in user else None
            doc['ROLE_ID'] = user['role'] if 'role'in user else "USER"
            doc['AVATAR'] = user['avatar'] if 'avatar' in user else None
            doc['ID'] = self.collection.find_one(sort=[('ID', -1)])['ID'] + 1
            print doc
            
            self.collection.insert(doc)
            return doc
        except:
            return {"message": 'Error'}


    def _valid_username(self, username=''):
        if len(username) < 30:
            if re.match(r"^[a-zA-Z][a-z0-9_.-]+$", username):
                return True
        else:
            return False


    def _valid_pass(self, password=''):
        if len(password) < 30:
            if re.match(r"^[a-zA-Z0-9][a-z0-9_.-]+$", password):
                return True
        else:
            return False


    def _valid_email(self, email=None):
        if len(email) < 50:
            if re.match(r"^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$", email):
                return True
        else:
            return False
    
