# -*- coding: utf-8 -*-
#!/usr/bin/env python

from flask import Flask

app = Flask(__name__)

app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='mikemongoheroku',
    USERNAME='michaluch',
    PASSWORD='576michaluch',
))


#from app.routes import users

from app.routes import (
    users,
    issues
)

if __name__ == '__main__':
    app.run()