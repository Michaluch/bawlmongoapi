#!/usr/bin/env python

from flask import (
    url_for,
    session,
    request,
    Response,
    json,
    jsonify
)

from app.appconfic import app
from app.config.config import MONGO_CREDENTIALS
from app.controllers.userController import UserController
from app.routes.routeutils import _send_list_JSON

_user = UserController(MONGO_CREDENTIALS)

@app.route("/", methods=["GET"])
def index():
    return _send_list_JSON({"message":"Welcome to balw application"})


@app.route("/users/all", methods=["GET"])
def get_all_ussers():
    """ This method return list of dictionary by all users
        in the system.
        Method return data in JSON format.
        
        Example:
        [{"usermane": "ahardy", "id": "3", "role": "ADMIN", "fullname": "Arnold Hardy"},
         {"usermane": "hgudin", "id": "1", "role": "MANAGER", "fullname": "Garry Gudini"},]
    """
    return _send_list_JSON(_user.get_list_all_users())
    #return Response(json.dumps(user.get_list_all_users()), mimetype="application/json", status=200)


@app.route("/users/current", methods=["GET"])
def get_current_user():
    """ This method return dictionary of current loginned
        users in the system.
        Method return data in JSON format.
        
        Example:
        {"usermane": "ahardy", "id": "3", "role": "ADMIN", "fullname": "Arnold Hardy"}
    """
    if 'logged_in' in session:
        userattr = {
            "ID": session['logged_in']
        }
        return _send_list_JSON(_user.get_user_by_atribute(**userattr))
    else:
        return _send_list_JSON({"message": "Something wrong!"})


@app.route("/users/auth/login", methods=["POST"])
def user_login():
    try:
        jsonbody = request.get_json()
        if _user.is_user(username=jsonbody.get('login'),
                         passwd=jsonbody.get('password')):
            session['logged_in'] = _user.get_id_user(
                jsonbody.get('login')
            )
            userattr = {
                "LOGIN": jsonbody.get('login')
                }
            return _send_list_JSON(_user.get_user_by_atribute(**userattr))
        else:
            return _send_list_JSON({"message": "Login or password incorect!"})
    except:
        return Response(jsonify(dict(message = "Something wrong!")), mimetype="application/json", status=200)

@app.route("/users/auth/logout", methods=["GET"])
def logout():
    session.pop('logged_in', None)
    return _send_list_JSON({"message": "Your is log out"})


@app.route("/users", methods=["POST"])
def add_new_user():
    jsonbody = request.get_json()
    userattr = _user.add_new_user(user=jsonbody)
    if  'message' not in userattr:
        return _send_list_JSON(_user.get_user_by_atribute(**userattr))
    else:
        return _send_list_JSON(userattr)

@app.route("/users/<int:id>", methods=["PUT"])
def edit_user():
    pass


@app.route("/users/validate", methods=["POST"])
def email_confirmation():
    pass


@app.route("/users/<int:id>", methods=["DELETE"])
def delete_user():
    pass


@app.route("/users/changepass", methods=["GET"])
def reset_user_password():
    pass
