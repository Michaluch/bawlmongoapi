#!/usr/bin/env python

from flask import (
    url_for,
    session,
    request,
    Response,
    json,
    jsonify
)

from app.appconfic import app
from app.config.config import MONGO_CREDENTIALS
from app.controllers.issueController import IssueController
from app.controllers.userController import UserController
from app.routes.routeutils import _send_list_JSON

_issue = IssueController(MONGO_CREDENTIALS)
_user = UserController(MONGO_CREDENTIALS)


@app.route("/issue/all", methods=["GET"])
def get_all_issues():
    return _send_list_JSON(_issue.get_all_issues())


@app.route("/issue/<int:issueid>", methods=["GET"])
def get_issue_by_id(issueid):
    jsonbody = {
        'ID': issueid
    }
    if _issue.get_issue_by_atribute(**jsonbody):
        
        return _send_list_JSON(_issue.get_issue_by_atribute(**jsonbody))
    else:
        return _send_list_JSON({"message": "Invalid issue id"})


@app.route("/issue/<int:issueid>/history", methods=["GET"])
def get_issue_history_by_issue_id(issueid):
    dirtylist = _issue.get_issue_history(issueid)
    for dictionary in dirtylist:
        for key, value in dictionary.items():
            if key == 'USER_ID':
                dictionary['USER'] = _user.get_user_by_id(value)
                del(dictionary[key])
    
    return _send_list_JSON(dirtylist)


@app.route("/issue/<int:id>/delete", methods=["DELETE"])
def delete_issue():
    pass


@app.route("/issue/resolved", methods=["GET"])
def get_resolved_issue():
    pass


@app.route("/issue", methods=["POST"])
def add_issue():
    jsonbody = request.get_json()
    issuedoc = _issue.add_new_issue(issue=jsonbody)
    if 'message' not in issuedoc:
       return _send_list_JSON(_issue.get_issue_by_atribute(**issuedoc))
    else:
       return _send_list_JSON(issuedoc)


@app.route("/issue/<int:id>", methods=["PUT"])
def edit_issue():
    pass


@app.route("/issue/<int:id>/resolve", methods=["PUT"])
def to_resolve_issue():
    pass
