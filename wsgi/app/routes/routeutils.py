from flask import (
    url_for,
    session,
    Response,
    json,
    jsonify,
)


def _send_list_JSON(instance=None):
    return Response(json.dumps(instance),
                    mimetype="application/json",
                    status=200)