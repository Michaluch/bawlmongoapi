#!/usr/bin/env python

from app.dao.mongoorm import MongoORM

class IssueModel(object):
    
    """ This class is used for schema of Issue entity """
    
    def __init__(self,credentials={}):
        self.collection = MongoORM(**credentials).case_collection('issues')
    