#!/usr/bin/env python

from app.dao.mongoorm import MongoORM
from bson.objectid import ObjectId
import re

class UserModel(object):
    """
    """

    def __init__(self, credentials={}):
        self.collection = MongoORM(**credentials).case_collection('users')